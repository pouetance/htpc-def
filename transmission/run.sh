docker run --name transmission \
    -p 9091:9091 -p 12345:12345 \
    -v /opt/docker/def/transmission/config/:/conf \
    -v /var/transmission-daemon/download:/download \
    -v /var/transmission-daemon/incomplete:/incomplete \
    -v /var/couchpotato/download:/download-couch \
    -v /var/sickrage/download:/download-sickrage \
    alex/transmission

